package com.comarch.comarch.services;

import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.services.confirmTranfer.ConfirmStrategy;
import com.comarch.comarch.services.confirmTranfer.ConfirmTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SchedulingService {

    private TransferService transferService;
    private AccountService accountService;
    private CurrencyService currencyService;
    private SimpMessageSendingOperations operations;
    @Autowired
    private MailService mailService;

    @Autowired
    public SchedulingService(TransferService transferService, AccountService accountService,
                             CurrencyService currencyService, SimpMessageSendingOperations operations) {
        this.transferService = transferService;
        this.accountService = accountService;
        this.currencyService = currencyService;
        this.operations = operations;
    }

    @Scheduled(fixedRate = 10000)
    public void acceptTransfer() {

        List<Transfer> transfers = transferService.getAllPendingTransfer();

        if (!transfers.isEmpty()) {
            operations.convertAndSend("/topic", "{message: ''}");
        }


        transfers.forEach(transfer ->
        {
            ConfirmStrategy strategy = ConfirmTemplate.of(transfer, currencyService);
            strategy.acceptTransfer(transfer);

            transferService.updateTransfer(transfer);
            accountService.updateAccount(transfer.getToAccount());
            if (transfer.getExternal() != null && !transfer.getExternal().equals("")) {
                mailService.promptAdmin("External prepareTransfer :" + transfer.getExternal());
            }
        });

    }
}
