package com.comarch.comarch.services;

import com.comarch.comarch.dao.UserDao;
import com.comarch.comarch.error.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@Qualifier("Sec")
public class UserDetailsServiceImpl implements UserDetailsService {


    private UserDao userDao;

    @Autowired
    public UserDetailsServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        com.comarch.comarch.entities.User myUserFromDb;

        myUserFromDb = userDao.findUserByName(username).orElseThrow(() -> new UserException(UserException.BAD_NAME));


        ArrayList authorities = new ArrayList() {{
            add(new SimpleGrantedAuthority("ROLE_USER"));
        }};


        UserDetails userDetails = new User(myUserFromDb.getName(), myUserFromDb.getPassword(), authorities);

        return userDetails;
    }
}
