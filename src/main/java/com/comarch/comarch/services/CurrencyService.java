package com.comarch.comarch.services;

import com.comarch.comarch.aspect.MyLog;
import com.comarch.comarch.currency.CurrencyInfo;
import com.comarch.comarch.currency.Rate;
import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.CurrencyEnum;
import com.comarch.comarch.entities.Transfer;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Service
public class CurrencyService {

    private RestTemplate restTemplate;
    private final String fooResourceUrl = "http://api.nbp.pl/api/exchangerates/rates/c/";
    private final String last = "/today/?format=json";

    public CurrencyService() {
        restTemplate = new RestTemplate();
    }

    @MyLog("kwota w obcej walucie")
    public BigDecimal amountWithCorrectCurrency(Transfer transfer, Account from) {
        BigDecimal transferAmount = transfer.getAmount();
        BigDecimal rate = getRateForCurrencyFromNbp(from.getCurrency(), transfer.getCurrency());
        BigDecimal correctAmount = changeAmountWithCurrencyRate(rate, transferAmount);
        return correctAmount;
    }

    private BigDecimal getRateForCurrencyFromNbp(CurrencyEnum from, CurrencyEnum to) {
        // TODO: 04.07.2019 api 2/10
        if (from == to) {
            return BigDecimal.ONE;
        }

        CurrencyInfo rate = restTemplate
                .getForObject(makeUrl(to == CurrencyEnum.PLN ? from : to), CurrencyInfo.class);
        Rate theRate = rate.getRates().get(0);
        BigDecimal rateAsBd = BigDecimal.valueOf(theRate.getBid());

        if (to == CurrencyEnum.PLN) {
            return new BigDecimal(1d / theRate.getBid());
        }
        return rateAsBd;
    }

    private BigDecimal changeAmountWithCurrencyRate(BigDecimal rate, BigDecimal amount) {
        return rate.multiply(amount);
    }

    private String makeUrl(CurrencyEnum currencyEnumName) {
        return fooResourceUrl + currencyEnumName.toString() + last;
    }
}
