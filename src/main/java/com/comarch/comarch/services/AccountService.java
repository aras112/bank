package com.comarch.comarch.services;

import com.comarch.comarch.aspect.MyLog;
import com.comarch.comarch.dao.AccountDao;
import com.comarch.comarch.dto.AccountDto;
import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.error.AccountException;
import com.comarch.comarch.services.mappers.impl.AccountMapper;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(isolation = Isolation.REPEATABLE_READ, propagation = Propagation.REQUIRED)
public class AccountService {

    private AccountDao accountDao;
    private AccountMapper mapper;

    @Autowired
    public AccountService(AccountDao accountDao, AccountMapper mapper) {
        this.accountDao = accountDao;
        this.mapper = mapper;
    }


    public Boolean accountWithNumberAlreadyExist(Account account) {
        Account accountExample = new Account();
        accountExample.setActive(null);
        accountExample.setAccountNumber(account.getAccountNumber());
        Example<Account> example = Example.of(accountExample, ExampleMatcher.matchingAll());
        return accountDao.exists(example);
    }

    @MyLog(value = "add account", level = Level.DEBUG)
    public void addAccount(AccountDto accountDto) {
        Account account = mapper.mapToEntity(accountDto);
        if (accountWithNumberAlreadyExist(account)) {
            throw new AccountException(AccountException.ACCOUNT_NOT_EXIST);
        }

        account.setActive(true);
        accountDao.save(account);
    }

    public AccountDto getAccountDtoByAccountNumber(String number) {
        Account account = getAccountByAccountNumber(number);
        return mapper.mapToDto(account);
    }

    public Account getAccountByAccountNumber(String number) {
        Account exampleAccount = new Account();
        exampleAccount.setAccountNumber(number);
        exampleAccount.setActive(null);
        Example<Account> example = Example.of(exampleAccount, ExampleMatcher.matchingAny());

        return accountDao.findOne(example).orElseThrow(() -> new AccountException(AccountException.ACCOUNT_NOT_EXIST));
    }

    public void deleteAccount(Long id) {
        Account account = getAccountById(id);
        account.setActive(false);
        updateAccount(account);
    }

    public List<AccountDto> getAllAccount() {
        return accountDao
                .findAll()
                .stream()
                .filter(Account::getActive)
                .map(mapper::mapToDto)
                .collect(Collectors.toList());
    }

    public Account getAccountById(Long id) {
        Optional<Account> account = accountDao.findById(id);
        return account.orElseThrow(() -> new AccountException(AccountException.ERROR_ID));
    }

    public AccountDto getAccountDtoById(Long id) {
        Account account = getAccountById(id);
        return mapper.mapToDto(account);
    }

    public Account updateAccount(Account account) {
        return accountDao.save(account);
    }

    public String getAccountAmount(Long id) {
        Account account = getAccountById(id);
        String amount = account.getAmount().toString();
        amount = addCurrencyToAmountString(account, amount);
        return amount;
    }

    @MyLog("get all account")
    public List<Transfer> getAllTransferOut(Long id) {
        Account account = getAccountById(id);
        return account.getTransfersOut();
    }

    public List<Transfer> getAllTransferIn(Long id) {
        Account account = getAccountById(id);
        return account.getTransfersIn();
    }

    private String addCurrencyToAmountString(Account account, String amount) {
        amount = amount + " " + account.getCurrency().toString();
        return amount;
    }
}
