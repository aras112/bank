package com.comarch.comarch.services.confirmTranfer.impl;

import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.services.confirmTranfer.ConfirmTemplate;

import java.math.BigDecimal;

public class LocalConfirm extends ConfirmTemplate
    {
    @Override
    protected void sendMoney(Transfer transfer, Account toAccount)
        {
        BigDecimal transferAmount = transfer.getAmount();
        toAccount.setAmount(toAccount.getAmount().add(transferAmount));
        }
    }
