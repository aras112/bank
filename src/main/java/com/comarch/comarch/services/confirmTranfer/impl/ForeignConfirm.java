package com.comarch.comarch.services.confirmTranfer.impl;

import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.services.CurrencyService;
import com.comarch.comarch.services.confirmTranfer.ConfirmStrategy;
import com.comarch.comarch.services.confirmTranfer.ConfirmTemplate;

import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

public class ForeignConfirm extends ConfirmTemplate
    {
    private CurrencyService currencyService;

    public ForeignConfirm(CurrencyService currencyService)
        {
        this.currencyService = currencyService;
        }

    @Override
    protected void sendMoney(Transfer transfer, Account toAccount)
        {
        BigDecimal transferAmount;
        transferAmount = currencyService.amountWithCorrectCurrency(transfer, toAccount);
        toAccount.setAmount(toAccount.getAmount().add(transferAmount));
        }
    }
