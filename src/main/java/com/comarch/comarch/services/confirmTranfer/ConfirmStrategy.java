package com.comarch.comarch.services.confirmTranfer;

import com.comarch.comarch.entities.Transfer;

public interface ConfirmStrategy
    {
    void acceptTransfer(Transfer transfer);
    }
