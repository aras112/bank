package com.comarch.comarch.services.confirmTranfer;

import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.entities.TransferType;
import com.comarch.comarch.services.CurrencyService;
import com.comarch.comarch.services.confirmTranfer.impl.ForeignConfirm;
import com.comarch.comarch.services.confirmTranfer.impl.LocalConfirm;

import java.time.LocalDateTime;

public abstract class ConfirmTemplate implements ConfirmStrategy
    {

    protected ConfirmTemplate()
        {
        }

    @Override
    public void acceptTransfer(Transfer transfer)
        {
        sendMoney(transfer, transfer.getToAccount());
        transfer.setTransferType(TransferType.END);
        transfer.setFinishTime(LocalDateTime.now());
        }

    public static ConfirmTemplate of(Transfer transfer, CurrencyService currencyService)
        {
        if (isLocalTransfer(transfer, transfer.getToAccount()))
            {
            return new LocalConfirm();
            }
        return new ForeignConfirm(currencyService);
        }

    private static boolean isLocalTransfer(Transfer transfer, Account account)
        {
        return transfer.getCurrency() == account.getCurrency();
        }

    protected abstract void sendMoney(Transfer transfer, Account toAccount);

    }
