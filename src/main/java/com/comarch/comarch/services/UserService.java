package com.comarch.comarch.services;

import com.comarch.comarch.aspect.MyLog;
import com.comarch.comarch.dao.UserDao;
import com.comarch.comarch.dto.UserPass;
import com.comarch.comarch.dto.UserRegisterDto;
import com.comarch.comarch.entities.User;
import com.comarch.comarch.error.UserException;
import com.comarch.comarch.services.mappers.impl.UserMapper;
import com.comarch.comarch.services.mappers.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserDao userDao;
    private UserMapper userMapper;
    private PasswordEncoder encoder;

    @Autowired
    public UserService(UserDao userDao, UserMapper userMapper, PasswordEncoder encoder) {
        this.userDao = userDao;
        this.userMapper = userMapper;
        this.encoder = encoder;
    }

    @MyLog("zapiswanie usera")
    public void saveUser(UserRegisterDto userDto) {

        if (userExistByName(userDto.getName())) {
            throw new UserException(UserException.USER_EXIST);
        }

        User user = userMapper.mapToUser(userDto);
        userDao.save(user);
    }

    public User findUserById(Long id) {
        Optional<User> user = userDao.findById(id);
        return user.orElseThrow(() -> new UserException(UserException.BAD_USER_ID));
    }

    public User findUserByName(String name) {
        return userDao.findUserByName(name).orElseThrow(() -> new UserException(UserException.BAD_NAME));
    }

    private boolean userExistByName(String name) {
        return userDao.findUserByName(name).isPresent();
    }

    @MyLog("LOGOWANIE DO SYSTEMU")
    public Long loginCorrectUser(UserPass userPass) {

        User user = findUserByName(userPass.getLogin());

        if (encoder.matches(userPass.getPassword(), user.getPassword())) {
            return user.getId();
        } else {
            throw new UserException(UserException.BAD_NAME);
        }
    }

    public void changeUserName(Long id, String name) {
        User user = findUserById(id);

        UserValidator.nameIsCorrectOrElseThrow(name);

        user.setName(name);
        userDao.save(user);
    }

}
