package com.comarch.comarch.services;

import com.comarch.comarch.aspect.MyLog;
import com.comarch.comarch.dao.TransferDao;
import com.comarch.comarch.dto.AccountDto;
import com.comarch.comarch.dto.ExternalTransferDto;
import com.comarch.comarch.dto.TransferDto;
import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.entities.TransferType;
import com.comarch.comarch.error.TransferException;
import com.comarch.comarch.services.mappers.Mappers;
import com.comarch.comarch.services.prepareTransfer.TransferStrategy;
import com.comarch.comarch.services.prepareTransfer.TransferTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
public class TransferService {

    private TransferDao transferDao;
    private AccountService accountService;
    private CurrencyService currencyService;
    private Mappers mappers;

    @Autowired
    public TransferService(TransferDao transferDao, AccountService accountService, CurrencyService currencyService, Mappers mappers) {
        this.transferDao = transferDao;
        this.accountService = accountService;
        this.currencyService = currencyService;
        this.mappers = mappers;
    }

    @Cacheable(value = "updateTransfer")
    public Transfer getTransferById(Long id) {
        return transferDao.findById(id).orElseThrow(() -> new TransferException(TransferException.ID_ERROR));
    }

    public List<Transfer> getAllPendingTransfer() {
        return transferDao.findAllByTransferType(TransferType.PENDING);
    }

    @CachePut(value = "updateTransfer", key = "#result.id", condition = "#transfer.transferType==T(com.comarch.comarch.entities.TransferType).END")
    public Transfer updateTransfer(Transfer transfer) {
        return transferDao.save(transfer);
    }

    @PreAuthorize("hasAnyRole('ROLE_USER')")
    public void prepareTransfer(TransferDto transferDto) {
        Transfer transfer = mappers.getTransferMapper().mapToTransferEntity(transferDto);

        TransferStrategy strategy = TransferTemplate.of(transfer, currencyService);
        strategy.prepareTransfer(transfer);

        updateTransfer(transfer);
        accountService.updateAccount(transfer.getFromAccount());
    }

    @MyLog(value = "serwis dla external prepareTransfer")
    public void externalTransfer(ExternalTransferDto transferDto) {

        Transfer transfer = mappers.getTransferMapper().mapToTransferEntity(transferDto);
        TransferStrategy strategy = TransferTemplate.of(transfer, currencyService);

        strategy.prepareTransfer(transfer);
        updateTransfer(transfer);
    }

    @Cacheable(value = "getAccountsNumberByTransferID")
    public List<AccountDto> getAccountsNumberByTransferID(Long id) {
        Transfer transfer = getTransferById(id);
        Account accountFrom = transfer.getFromAccount();
        Account accountTo = transfer.getToAccount();
        ArrayList<AccountDto> accounts = new ArrayList<>();
        accounts.add(mappers.getAccountMapper().mapToDto(accountFrom));
        accounts.add(mappers.getAccountMapper().mapToDto(accountTo));
        return accounts;
    }
}
