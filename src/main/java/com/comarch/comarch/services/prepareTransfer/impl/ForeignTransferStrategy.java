package com.comarch.comarch.services.prepareTransfer.impl;

import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.services.CurrencyService;
import com.comarch.comarch.services.prepareTransfer.TransferTemplate;

import java.math.BigDecimal;

public class ForeignTransferStrategy extends TransferTemplate
    {

    private CurrencyService currencyService;

    public ForeignTransferStrategy(CurrencyService currencyService)
        {
        this.currencyService = currencyService;
        }

    @Override
    public void subtractMoneyFromSenderAccount(Transfer transfer, Account fromAccount)
        {
        BigDecimal transferAmount = currencyService.amountWithCorrectCurrency(transfer, fromAccount);
        hasEnoughMoneyInAccountElseThrowException(fromAccount, transferAmount);
        fromAccount.setAmount(fromAccount.getAmount().subtract(transferAmount));
        }
    }
