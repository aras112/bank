package com.comarch.comarch.services.prepareTransfer.impl;

import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.services.prepareTransfer.TransferTemplate;

import java.math.BigDecimal;

public class LocalTransferStrategy extends TransferTemplate
    {
    @Override
    public void subtractMoneyFromSenderAccount(Transfer transfer, Account from)
        {
        // TODO: 03.07.2019 czy user id z from sie nie jest taki sam jak Account to
        BigDecimal transferAmount = transfer.getAmount();
        hasEnoughMoneyInAccountElseThrowException(from, transferAmount);
        from.setAmount(from.getAmount().subtract(transferAmount));
        }
    }
