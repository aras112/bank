package com.comarch.comarch.services.prepareTransfer;

import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.entities.TransferType;
import com.comarch.comarch.error.TransferException;
import com.comarch.comarch.services.CurrencyService;
import com.comarch.comarch.services.prepareTransfer.impl.ExternalTransferStrategy;
import com.comarch.comarch.services.prepareTransfer.impl.ForeignTransferStrategy;
import com.comarch.comarch.services.prepareTransfer.impl.LocalTransferStrategy;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public abstract class TransferTemplate implements TransferStrategy {

    //-------------
    protected TransferTemplate() {
    }

    //-------------
    public static TransferTemplate of(Transfer transfer, CurrencyService currencyService) {
        if (isExternalTransfer(transfer)) {
            return new ExternalTransferStrategy();
        } else if (isLocalTransfer(transfer, transfer.getFromAccount())) {
            return new LocalTransferStrategy();
        }
        return new ForeignTransferStrategy(currencyService);
    }

    private static boolean isExternalTransfer(Transfer transfer) {
        return transfer.getTransferType().equals(TransferType.EXTERNAL);
    }

    private static boolean isLocalTransfer(Transfer transfer, Account account) {
        return transfer.getCurrency() == account.getCurrency();
    }

    public abstract void subtractMoneyFromSenderAccount(Transfer transfer, Account from);

    @Override
    final public Transfer prepareTransfer(Transfer transfer) {
        Account from = transfer.getFromAccount();

        setDateAndStatusForTransfer(transfer);

        subtractMoneyFromSenderAccount(transfer, from);

        return transfer;
    }

    final protected void hasEnoughMoneyInAccountElseThrowException(Account from, BigDecimal transferAmount) {
        if (notEnoughMoney(from, transferAmount)) {
            throw new TransferException(TransferException.MONEY_ERROR);
        }
    }

    private void setDateAndStatusForTransfer(Transfer transfer) {
        transfer.setAddedTime(LocalDateTime.now());
        transfer.setTransferType(TransferType.PENDING);
    }

    private boolean notEnoughMoney(Account from, BigDecimal transferAmount) {
        return transferAmount.compareTo(from.getAmount()) >= 1;
    }
}
