package com.comarch.comarch.services.prepareTransfer;

import com.comarch.comarch.entities.Transfer;

public interface TransferStrategy
    {
    Transfer prepareTransfer(Transfer transfer);
    }
