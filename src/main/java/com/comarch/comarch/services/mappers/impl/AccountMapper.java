package com.comarch.comarch.services.mappers.impl;

import com.comarch.comarch.dto.AccountDto;
import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.User;
import com.comarch.comarch.services.UserService;
import com.comarch.comarch.services.mappers.validators.AccountValidator;
import com.comarch.comarch.services.mappers.validators.TransferValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


@Component
public class AccountMapper {

    private UserService userService;

    @Autowired
    public AccountMapper(UserService userService) {
        this.userService = userService;
    }

    public AccountDto mapToDto(Account account) {
        ModelMapper modelMapper = new ModelMapper();
        AccountDto dto = modelMapper.map(account, AccountDto.class);
        dto.setUserId(account.getUser().getId());
        return dto;
    }

    public Account mapToEntity(AccountDto account) {

        AccountValidator.accountHas26DigitOrElseThrow(account.getAccountNumber());
        TransferValidator.currencyIsCorrectOrElseThrow(account.getCurrency());

        ModelMapper modelMapper = new ModelMapper();
        Account entity = modelMapper.map(account, Account.class);
        entity.setAmount(BigDecimal.ZERO);


        User user = userService.findUserById(account.getUserId());
        entity.setUser(user);
        return entity;
    }

}
