package com.comarch.comarch.services.mappers.validators.custom;


import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AccountNumberValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccountNumber {

    String message() default "invalid account number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

class AccountNumberValidator implements ConstraintValidator<AccountNumber, String> {

    private static final String REGEX_ACCOUNT_NUMBER = "\\d{26}";

    @Override
    public void initialize(AccountNumber constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return false;
        }
        return value.matches(REGEX_ACCOUNT_NUMBER);
    }
}
