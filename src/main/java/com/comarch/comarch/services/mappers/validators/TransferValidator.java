package com.comarch.comarch.services.mappers.validators;

import com.comarch.comarch.entities.CurrencyEnum;
import com.comarch.comarch.error.AccountException;
import com.comarch.comarch.error.TransferException;

public class TransferValidator {

    private static final String BANK_REGEX = "(\\w)+[0-9]{2}$";
    private static final String AMOUNT_REGEX = "^\\d{1,6}([,.][0-9]{1,2})?";
    private static final String ACCOUNT_REGEX = "\\d+";

    private static boolean accountHas26Digit(String number) {
        return number.length() == 26 && number.matches(ACCOUNT_REGEX);
    }

    public static void accountHas26DigitOrElseThrow(String number) {
        if (number == null || !accountHas26Digit(number)) {
            throw new AccountException(AccountException.ACCOUNT_NUMBER_ERROR);
        }
    }

    public static void currencyIsCorrectOrElseThrow(String currency) {
        try {
            CurrencyEnum.valueOf(currency);
        } catch (Throwable e) {
            throw new TransferException(TransferException.CURRENCY_ERROR);
        }
    }

    public static void transferAmountIsCorrectOrElseThrow(String amount) {
        if (amount == null || !amount.matches(AMOUNT_REGEX)) {
            throw new TransferException(TransferException.AMOUNT_FORMAT_ERROR);
        }
    }

    public static void externalAccountNumberAndBankNameIsCorrectOrElseThrow(String number, String bankName) {
        if (number == null || bankName == null || !bankName.matches(BANK_REGEX)) {
            throw new TransferException(TransferException.BANK_FORMAT_ERROR);
        }
    }


}
