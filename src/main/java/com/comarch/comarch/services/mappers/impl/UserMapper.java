package com.comarch.comarch.services.mappers.impl;

import com.comarch.comarch.aspect.MyLog;
import com.comarch.comarch.dto.UserRegisterDto;
import com.comarch.comarch.entities.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static com.comarch.comarch.services.mappers.validators.UserValidator.*;

@Component
public class UserMapper {

    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    @MyLog("mapowanie do usera")
    public User mapToUser(UserRegisterDto registerDto) {
        ModelMapper modelMapper = new ModelMapper();
        valid(registerDto);
        User user = modelMapper.map(registerDto, User.class);

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return user;
    }

    private void valid(UserRegisterDto dto) {
        nameIsCorrectOrElseThrow(dto.getName());
        ageIsCorrectOrElseThrow(dto.getAge());
        emailIsCorrectOrElseThrow(dto.getEmail());
        passwordIsCorrectOrElseThrow(dto.getPassword());
        surnameIsCorrectOrElseThrow(dto.getSurname());
    }

}
