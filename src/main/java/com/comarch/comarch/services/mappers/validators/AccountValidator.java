package com.comarch.comarch.services.mappers.validators;

import com.comarch.comarch.error.AccountException;

public class AccountValidator {

    private static final String ACCOUNT_REGEX = "\\d+";

    private static boolean accountHas26Digit(String number) {
        return number.length() == 26 && number.matches(ACCOUNT_REGEX);
    }

    public static void accountHas26DigitOrElseThrow(String number) {
        if (number == null || !accountHas26Digit(number)) {
            throw new AccountException(AccountException.ACCOUNT_NUMBER_ERROR);
        }
    }

}
