package com.comarch.comarch.services.mappers.validators;

import com.comarch.comarch.error.UserException;

public class UserValidator {

    private static final String EMAIL_REGEXP = "^[\\w,.]+[@]\\w+([.]\\w+)+$";

    public static void nameIsCorrectOrElseThrow(String name) {
        if (name == null || name.equals("")) {
            throw new UserException(UserException.BAD_NAME);
        }
    }


    public static void surnameIsCorrectOrElseThrow(String surname) {
        if (surname == null || surname.equals("")) {
            throw new UserException(UserException.BAD_SURNAME);
        }
    }

    public static void ageIsCorrectOrElseThrow(Integer age) {
        if (age == null || age > 110 || age < 18) {
            throw new UserException(UserException.BAD_AGE);
        }
    }

    public static void emailIsCorrectOrElseThrow(String email) {
        if (email == null || !email.matches(EMAIL_REGEXP)) {
            throw new UserException(UserException.BAD_EMAIL);
        }
    }

    public static void passwordIsCorrectOrElseThrow(String password) {
        if (password == null || password.length() < 6) {
            throw new UserException(UserException.BAD_PASSWORD);
        }
    }
}
