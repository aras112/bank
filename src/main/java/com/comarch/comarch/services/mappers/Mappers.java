package com.comarch.comarch.services.mappers;

import com.comarch.comarch.services.mappers.impl.AccountMapper;
import com.comarch.comarch.services.mappers.impl.TransferMapper;
import com.comarch.comarch.services.mappers.impl.UserMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Data
public class Mappers {

    private AccountMapper accountMapper;
    private TransferMapper transferMapper;
    private UserMapper userMapper;

    @Autowired
    public Mappers(AccountMapper accountMapper, TransferMapper transferMapper, UserMapper userMapper) {
        this.accountMapper = accountMapper;
        this.transferMapper = transferMapper;
        this.userMapper = userMapper;
    }
}
