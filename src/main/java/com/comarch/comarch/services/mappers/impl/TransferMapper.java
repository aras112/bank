package com.comarch.comarch.services.mappers.impl;

import com.comarch.comarch.aspect.MyLog;
import com.comarch.comarch.dto.ExternalTransferDto;
import com.comarch.comarch.dto.TransferDto;
import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.CurrencyEnum;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.entities.TransferType;
import com.comarch.comarch.services.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static com.comarch.comarch.services.mappers.validators.TransferValidator.*;

@Component
public class TransferMapper {

    private AccountService accountService;

    @Autowired
    public TransferMapper(AccountService accountService) {
        this.accountService = accountService;
    }

    @MyLog("mapowanie transferu z external_transfer_dto")
    public Transfer mapToTransferEntity(ExternalTransferDto transferDto) {
        Transfer transfer = new Transfer();

        validData(transferDto);

        Account account = accountService.getAccountByAccountNumber(transferDto.getToAccount());

        transfer.setToAccount(account);
        transfer.setTransferType(TransferType.EXTERNAL);
        transfer.setAmount(new BigDecimal(transferDto.getAmount()));
        transfer.setCurrency(CurrencyEnum.valueOf(transferDto.getCurrency()));
        transfer.setExternal(transferDto.getBankName() + " " + transferDto.getExternalAccount());

        return transfer;
    }

    private void validData(ExternalTransferDto transferDto) {
        accountHas26DigitOrElseThrow(transferDto.getToAccount());
        currencyIsCorrectOrElseThrow(transferDto.getCurrency());
        transferAmountIsCorrectOrElseThrow(transferDto.getAmount());
        externalAccountNumberAndBankNameIsCorrectOrElseThrow(transferDto.getExternalAccount(), transferDto.getBankName());
    }

    public Transfer mapToTransferEntity(TransferDto transfer) {
        ModelMapper modelMapper = new ModelMapper();
        Transfer transferEntity = modelMapper.map(transfer, Transfer.class);
        Account from = accountService.getAccountById(transfer.getFromAccount());
        Account to = accountService.getAccountById(transfer.getToAccount());

        transferEntity.setFromAccount(from);
        transferEntity.setToAccount(to);
        transferEntity.setTransferType(TransferType.ADDED);

        return transferEntity;
    }

    private TransferDto mapToTransferDto(Transfer transfer) {
        ModelMapper modelMapper = new ModelMapper();
        TransferDto dto = modelMapper.map(transfer, TransferDto.class);
        dto.setFromAccount(transfer.getFromAccount().getId());
        dto.setToAccount(transfer.getToAccount().getId());
        return dto;
    }

}
