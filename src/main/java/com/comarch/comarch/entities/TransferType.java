package com.comarch.comarch.entities;

public enum TransferType {
    ADDED, PENDING, END, EXTERNAL
}
