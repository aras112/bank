package com.comarch.comarch.entities;

import com.comarch.comarch.services.mappers.validators.custom.AccountNumber;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(targetEntity = User.class)
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "fromAccount")
    private List<Transfer> transfersOut;

    @JsonIgnore
    @OneToMany(mappedBy = "toAccount")
    private List<Transfer> transfersIn;

    @AccountNumber
    @Column(unique = true)
    private String accountNumber;

    @Enumerated(EnumType.STRING)
    private CurrencyEnum currency;

    @DecimalMin(value = "0.0", message = "money less than 0")
    private BigDecimal amount;

    @NotNull(message = "cant be null")
    private Boolean active = false;

}
