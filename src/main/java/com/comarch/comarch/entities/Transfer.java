package com.comarch.comarch.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = {"fromAccount","toAccount"})
public class Transfer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    private CurrencyEnum currency;

    @Enumerated(EnumType.STRING)
    private TransferType transferType;

    private LocalDateTime addedTime;

    private LocalDateTime finishTime;

    @DecimalMin("0.0")
    private BigDecimal amount;

    @ManyToOne(targetEntity = Account.class,cascade = CascadeType.REFRESH)
    private Account fromAccount;

    @ManyToOne(targetEntity = Account.class,cascade = CascadeType.REFRESH)
    private Account toAccount;

    @Column(name = "EXTERNAL_NAME")
    private String external;

}
