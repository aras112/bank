package com.comarch.comarch.entities;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

public class ForeignAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Pattern(regexp = "\\d{26}", message = "account number doesnt have correct length")
    @Column(unique = true)
    private String accountNumber;

    private String bankName;
}
