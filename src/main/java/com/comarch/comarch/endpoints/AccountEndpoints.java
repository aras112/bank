package com.comarch.comarch.endpoints;

import com.comarch.comarch.dto.AccountDto;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.services.AccountService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/account", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@CrossOrigin
@Slf4j
public class AccountEndpoints {

    @Autowired
    private AccountService accountService;


    @ApiOperation("add account")
    @PostMapping("/add")
    @ResponseStatus(HttpStatus.OK)
    public void addAccount(@RequestBody AccountDto account) {
        log.debug("account add request : \n" + account.toString());
        accountService.addAccount(account);
    }

    @ApiOperation("delete account")
    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteAccount(@PathVariable Long id) {
        log.debug("delete account with id : \n" + id);
        accountService.deleteAccount(id);
    }

    @ApiOperation("get account amount")
    @GetMapping("/amount/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String getAccountAmountById(@PathVariable Long id) {
        log.debug("account amount request for id : \n" + id);
        String amount = accountService.getAccountAmount(id);
        return amount;
    }

    @ApiOperation("get account")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AccountDto getAccountById(@PathVariable Long id) {
        log.debug("account dto request for id : \n" + id);
        AccountDto account = accountService.getAccountDtoById(id);
        return account;
    }

    @ApiOperation("get account by number")
    @GetMapping("/number/{number}")
    @ResponseStatus(HttpStatus.OK)
    public AccountDto getAccountByAccountNumber(@PathVariable String number) {
        log.debug("get account by account name number : " + number);
        return accountService.getAccountDtoByAccountNumber(number);
    }

    @ApiOperation("get account transfers out")
    @GetMapping("/transfer/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<Transfer> getAccountTransfersOutById(@PathVariable Long id) {
        log.debug("show transfers request for id : \n" + id);
        List<Transfer> transfers = accountService.getAllTransferOut(id);
        return transfers;
    }

    @ApiOperation("get account transfers in")
    @GetMapping("/transferIn/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<Transfer> getAccountTransfersInById(@PathVariable Long id) {
        log.debug("show transfers request for id : \n" + id);
        List<Transfer> transfers = accountService.getAllTransferIn(id);
        return transfers;
    }

    @ApiOperation("get all account")
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<AccountDto> getAllAccounts() {
        log.debug("get all account: \n");
        List<AccountDto> accounts = accountService.getAllAccount();
        return accounts;
    }

}
