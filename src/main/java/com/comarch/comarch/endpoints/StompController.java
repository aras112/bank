package com.comarch.comarch.endpoints;

import com.comarch.comarch.aspect.MyLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
@Slf4j
public class StompController {

    @MessageMapping("/aa")
    @SendTo("/topic")
    @MyLog("STOMP CONNECTED")
    String stomp() {
        return "STOMP CONNECTED";
    }

}
