package com.comarch.comarch.endpoints;

import com.comarch.comarch.aspect.MyLog;
import com.comarch.comarch.dto.AccountDto;
import com.comarch.comarch.dto.ExternalTransferDto;
import com.comarch.comarch.dto.TransferDto;
import com.comarch.comarch.services.TransferService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping(path = "/transfer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class TransferEndpoints {

    @Autowired
    private TransferService transferService;

    @PostMapping("/send")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Transfer money")
    public void transfer(@RequestBody TransferDto transferDto) {
        log.debug("prepareTransfer request : \n" + transferDto.toString());
        transferService.prepareTransfer(transferDto);
    }

    @ApiOperation(value = "Transfer money from external bank")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Transfer success"),
            @ApiResponse(code = 400, message = "Account doesnt exist")
    })
    @PostMapping("/external-transfer")
    @ResponseStatus(HttpStatus.OK)
    @MyLog("external prepareTransfer endpoint")
    public void externalTransfer(@RequestBody ExternalTransferDto transferDto) {
        log.debug("prepareTransfer request : \n" + transferDto.toString());
        transferService.externalTransfer(transferDto);
    }


    @GetMapping("/accounts/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<AccountDto> getAccountByTransferId(@PathVariable Long id) {

        log.debug("get account by prepareTransfer id : " + id);
        return transferService.getAccountsNumberByTransferID(id);
    }

}
