package com.comarch.comarch.endpoints;

import com.comarch.comarch.dto.UserPass;
import com.comarch.comarch.dto.UserRegisterDto;
import com.comarch.comarch.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
public class UserEndpoints {

    @Autowired
    private UserService userService;

    @PostMapping("/changeName/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void changeUSerName(@PathVariable Long id, @RequestParam String name) {
        log.debug("change user name");
        userService.changeUserName(id, name);
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public Long login(@RequestBody UserPass userPass) {
        log.debug("change user name");
        return userService.loginCorrectUser(userPass);
    }

    @PostMapping("/addUser")
    @ResponseStatus(HttpStatus.CREATED)
    public void addUser(@RequestBody UserRegisterDto registerDto) {
        userService.saveUser(registerDto);
    }

}
