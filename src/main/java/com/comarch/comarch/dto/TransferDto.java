package com.comarch.comarch.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransferDto {
    private Long id;
    private String currency;
    private String amount;
    private Long fromAccount;
    private Long toAccount;
    private String external;
}


