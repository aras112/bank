package com.comarch.comarch.dto;

import lombok.Data;

@Data
public class StompMessageDto {
    private String message;
}
