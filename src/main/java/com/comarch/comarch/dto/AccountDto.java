package com.comarch.comarch.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class AccountDto
    {
    private Long id;
    private Long userId;
    private String accountNumber;
    private String currency;
    private String amount;
    private Boolean active = false;
    }


