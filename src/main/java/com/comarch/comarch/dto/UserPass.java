package com.comarch.comarch.dto;

import lombok.Data;

@Data
public class UserPass {
    private String login;
    private String password;
}
