package com.comarch.comarch.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExternalTransferDto {
    @ApiModelProperty(required = true, example = "PLN", allowableValues = "PLN, EUR")
    private String currency;
    @ApiModelProperty(required = true, example = "100", allowableValues = "[1, 1000000]")
    private String amount;
    @ApiModelProperty(required = true, example = "12345678901234567890123451")
    private String externalAccount;
    @ApiModelProperty(required = true, example = "12345678901234567890123451")
    private String toAccount;
    @ApiModelProperty(required = true, example = "MyBank44")
    private String bankName;
}
