package com.comarch.comarch.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRegisterDto {
    private String name;
    private String surname;
    private Integer age;
    private Boolean active = false;
    private String email;
    private String password;
}
