package com.comarch.comarch.error;

public class UserException extends RuntimeException {

    public static final String BAD_SURNAME = "surname is incorrect";
    public static final String BAD_AGE = "age is incorrect";
    public static final String BAD_EMAIL = "email is incorrect";
    public static final String BAD_PASSWORD = "password incorrect, has more than 6 digit?";
    public static final String BAD_USER_ID = "this user id is incorrect ";
    public static final String BAD_NAME = "name is incorrect";
    public static final String USER_EXIST = "user already exist";

    public UserException(String message) {
        super(message);
    }
}
