package com.comarch.comarch.error;

public class TransferException extends RuntimeException {

    public static String MONEY_ERROR = "Not enough money";
    public static String ID_ERROR = "Id is incorrect";
    public static String CURRENCY_ERROR = "Currency is incorrect, only [EUR, PLN] are correct";
    public static String AMOUNT_FORMAT_ERROR = "Amount format is incorrect";
    public static String BANK_FORMAT_ERROR = "Bank format is incorrect";

    public TransferException(String message) {
        super(message);
    }
}
