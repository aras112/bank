package com.comarch.comarch.error;

public class AccountException extends RuntimeException {

    public static String ERROR_ID = "this account doesnt exist";
    public static String ACCOUNT_NUMBER_ERROR = "Number doesnt have 26 numbers";
    public static String ACCOUNT_NOT_EXIST = "Account doesnt exist";

    public AccountException(String message) {
        super(message);
    }
}
