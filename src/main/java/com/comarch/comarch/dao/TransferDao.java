package com.comarch.comarch.dao;

import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.entities.TransferType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransferDao extends JpaRepository<Transfer,Long> {
    List<Transfer> findAllByTransferType(TransferType transferType);
}
