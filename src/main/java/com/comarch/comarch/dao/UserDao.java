package com.comarch.comarch.dao;

import com.comarch.comarch.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDao extends JpaRepository<User,Long>{
    Optional<User> findUserByName(String name);
}
