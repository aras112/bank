package com.comarch.comarch.settings;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.builders.PathSelectors.ant;

@Configuration
@EnableSwagger2
public class Swagger {

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("bankAPI")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(getPaths())
                .build()
                .globalResponseMessage(RequestMethod.GET, newArrayList(new ResponseMessageBuilder()
                        .code(500)
                        .message("500")
                        .responseModel(new ModelRef("error")).build()));
    }

    private Predicate<String> getPaths() {
        return or(ant("/prepareTransfer/external*"));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Bank")
                .description("MyBank")
                .contact(new Contact("Arkadiusz",
                        "https://frontcomarch.herokuapp.com/login",
                        "http://localhost:8080/swagger-ui.html"))
                .build();
    }

}
