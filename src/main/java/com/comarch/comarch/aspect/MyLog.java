package com.comarch.comarch.aspect;

import org.slf4j.event.Level;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyLog {
    String value() default "method";

    Level level() default Level.INFO;
}
