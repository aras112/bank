package com.comarch.comarch.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.slf4j.event.Level;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
public class LogAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private String input;
    private String returnInput;

    @Pointcut("within(com.comarch.comarch..*)")
    public void inPackage() {
    }

    @Pointcut("@annotation(myLog)")
    public void isAnnotatedByLog(MyLog myLog) {
    }

    @Around("isAnnotatedByLog(myLog) && inPackage()")
    public Object log(ProceedingJoinPoint point, MyLog myLog) throws Throwable {
        StringBuilder builder = new StringBuilder();
        Object proceed;
        Marker marker;

        /*get all args to string*/

        if (point.getArgs().length == 0) {
            builder.append("VOID");
        } else {
            for (Object o : point.getArgs()) {

                if (o != null) {
                    try {
                        builder.append(o.toString());
                        builder.append(" & ");
                    } catch (Exception e) {
                        builder.append("- arg lazy or null - & ");
                    }
                }
            }

            builder.deleteCharAt(builder.lastIndexOf("&"));
        }
        Level level = myLog.level();
        marker = MarkerFactory.getIMarkerFactory().getMarker(myLog.value());


        Method loggerWithLevel = getLogger(level);

        input = makeInput(point, builder, myLog);

        try {
            proceed = point.proceed();
        } catch (Throwable e) {
            logger.error(input + "\n--ERROR--\n" + e.toString() + "\n--ERROR--\n");
            throw e;
        }


        if (proceed != null) {
            returnInput = makeReturn(proceed);
            loggerWithLevel.invoke(this.logger, marker, input + returnInput);
        } else {
            loggerWithLevel.invoke(this.logger, marker, input);
        }

        return proceed;
    }

    private String makeReturn(Object proceed) {
        return "\n-RETURN-\nreturn " + proceed.getClass().getSimpleName() + " " + proceed.toString() + "\n-RETURN-\n";
    }

    private String makeInput(ProceedingJoinPoint point, StringBuilder builder, MyLog log) {
        return "\n\n--MyLog--\n" + log.value().toUpperCase() + " \n"
                + point.getSignature().getName()
                + "()\nwith args " + builder.toString()
                + "\nin class " + point.getTarget().getClass().getSimpleName()
                + "\n---------\n";
    }

    private Method getLogger(Level level) throws NoSuchMethodException {
        return logger.getClass().getMethod(level.toString().toLowerCase(), Marker.class, String.class);
    }
}
