package com.comarch.comarch.services;

import com.comarch.comarch.dao.TransferDao;
import com.comarch.comarch.dto.TransferDto;
import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.CurrencyEnum;
import com.comarch.comarch.entities.Transfer;
import com.comarch.comarch.error.TransferException;
import com.comarch.comarch.services.mappers.Mappers;
import com.comarch.comarch.services.mappers.impl.TransferMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    @Mock
    TransferDao transferDao;
    private static final String PLN_TO_EURO = "4";
    @Mock
    AccountService accountService;
    @Mock
    CurrencyService currencyService;
    @Mock
    Mappers mappers;
    @Mock
    TransferMapper transferMapper;
    private TransferService transferService;


    @Before
    public void setUp() {
        transferService = new TransferService(transferDao, accountService, currencyService, mappers);
        when(mappers.getTransferMapper()).thenReturn(transferMapper);

        doAnswer(this::simplePlnToEuroConvert)
                .when(currencyService)
                .amountWithCorrectCurrency(any(), any());
    }

    @Test
    public void givenAccountWith10Pln_whenUserPrepareTransferWithOnePln_thenHasOnly9PlnInAccount() {
        //g
        Account from = BuilderTest.getFromAccount()
                .currency(CurrencyEnum.PLN)
                .amount(BigDecimal.TEN)
                .build();

        Account to = BuilderTest.getToAccount().build();

        Transfer transfer = BuilderTest.getTransfer(from, to).build();
        transfer.setAmount(BigDecimal.ONE);

        //w
        when(transferMapper.mapToTransferEntity(any(TransferDto.class))).thenReturn(transfer);
        transferService.prepareTransfer(transferDto());

        //t
        assertThat(from.getAmount(), is(BigDecimal.valueOf(9)));
    }

    @Test
    public void givenAccountWith10Pln_whenUserPrepareTransferWithOneEur_thenUserHas6Eur() {
        //g
        Account from = BuilderTest.getFromAccount()
                .currency(CurrencyEnum.PLN)
                .amount(BigDecimal.TEN)
                .build();

        Account to = BuilderTest.getToAccount().build();

        Transfer transfer = BuilderTest.getTransfer(from, to)
                .amount(BigDecimal.ONE)
                .currency(CurrencyEnum.EUR)
                .build();

        when(transferMapper.mapToTransferEntity(any(TransferDto.class))).thenReturn(transfer);

        //w
        transferService.prepareTransfer(transferDto());

        //t
        assertThat(from.getAmount(), is(BigDecimal.valueOf(6)));
    }

    @Test(expected = TransferException.class)
    public void givenAccountWith10Pln_whenUserPrepareTransferWithFourEuro_thenUserGetException$NOT_ENOUGH_MONEY() {
        //g
        Account from = BuilderTest.getFromAccount()
                .currency(CurrencyEnum.PLN)
                .amount(BigDecimal.TEN)
                .build();

        Account to = BuilderTest.getToAccount().build();

        Transfer transfer = BuilderTest.getTransfer(from, to)
                .amount(BigDecimal.valueOf(4))
                .currency(CurrencyEnum.EUR)
                .build();

        when(transferMapper.mapToTransferEntity(any(TransferDto.class))).thenReturn(transfer);

        //w
        transferService.prepareTransfer(transferDto());

        //t not enough money -> expected = TransferException.class
    }

    @Test
    public void givenPlnAccount_whenUserPrepareEuroTransfer_thenCurrencyServiceRunsOneTime() {
        //g
        Account from = BuilderTest.getFromAccount()
                .currency(CurrencyEnum.PLN)
                .build();

        Account to = BuilderTest.getToAccount().build();

        Transfer transfer = BuilderTest.getTransfer(from, to)
                .currency(CurrencyEnum.EUR)
                .build();

        when(transferMapper.mapToTransferEntity(any(TransferDto.class))).thenReturn(transfer);

        //w
        transferService.prepareTransfer(transferDto());

        //t
        verify(currencyService, times(1)).amountWithCorrectCurrency(any(), any());

    }

    @Test
    public void givenPlnAccount_whenUserPreparePlnTransfer_thenCurrencyServiceRunsZeroTime() {
        //g
        Account from = BuilderTest.getFromAccount()
                .currency(CurrencyEnum.PLN)
                .build();

        Account to = BuilderTest.getToAccount().build();

        Transfer transfer = BuilderTest.getTransfer(from, to)
                .currency(CurrencyEnum.PLN)
                .build();

        when(transferMapper.mapToTransferEntity(any(TransferDto.class))).thenReturn(transfer);

        //w
        transferService.prepareTransfer(transferDto());

        //t
        verify(currencyService, times(0)).amountWithCorrectCurrency(any(), any());

    }


    private BigDecimal simplePlnToEuroConvert(InvocationOnMock invocation) {
        return ((Transfer) (invocation.getArgument(0))).getAmount().multiply(new BigDecimal(PLN_TO_EURO));
    }

    private TransferDto transferDto() {
        return TransferDto.builder().build();
    }
}
