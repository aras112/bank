package com.comarch.comarch.services;

import com.comarch.comarch.dto.TransferDto;
import com.comarch.comarch.dto.UserRegisterDto;
import com.comarch.comarch.entities.*;

import java.math.BigDecimal;
import java.util.Arrays;

public class BuilderTest {

    public static Account.AccountBuilder getFromAccount() {
        return Account.builder()
                .accountNumber("12345678901234567890123456")
                .currency(CurrencyEnum.PLN)
                .amount(BigDecimal.TEN);
    }

    public static Account.AccountBuilder getToAccount() {
        return Account.builder()
                .accountNumber("12345678901234567890123456")
                .currency(CurrencyEnum.PLN)
                .amount(BigDecimal.TEN);
    }

    public static TransferDto getTransferDto() {
        return TransferDto.builder()
                .amount("1")
                .currency("PLN")
                .fromAccount(1L)
                .toAccount(2L)
                .build();
    }

    public static Transfer.TransferBuilder getTransfer(Account from, Account to) {
        return Transfer.builder()
                .fromAccount(from)
                .toAccount(to)
                .currency(CurrencyEnum.PLN)
                .amount(new BigDecimal("1"))
                .transferType(TransferType.ADDED);
    }

    public static User.UserBuilder getUser(Account... a) {
        return User.builder()
                .age(20)
                .active(true)
                .accounts(Arrays.asList(a))
                .name("arek")
                .surname("kowalski")
                .password("password")
                .id(1L)
                .email("aras@2o.pl");
    }

    public static UserRegisterDto.UserRegisterDtoBuilder getUserDto() {
        return UserRegisterDto.builder()
                .name("arek")
                .surname("kowalski")
                .password("password")
                .active(true)
                .age(20)
                .email("aras@2o.pl");
    }

}
