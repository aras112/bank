package com.comarch.comarch.services;

import com.comarch.comarch.dao.UserDao;
import com.comarch.comarch.dto.UserRegisterDto;
import com.comarch.comarch.entities.Account;
import com.comarch.comarch.entities.User;
import com.comarch.comarch.error.UserException;
import com.comarch.comarch.services.mappers.impl.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private UserService userService;

    @Mock
    private UserDao userDao;
    @Mock
    private UserMapper userMapper;
    @Mock
    private PasswordEncoder encoder;

    private Account account = BuilderTest.getFromAccount().build();
    private User user = BuilderTest.getUser(account).build();
    private UserRegisterDto registerDto = BuilderTest.getUserDto().build();

    @Before
    public void setUp() {
        userService = new UserService(userDao, userMapper, encoder);
        when(userDao.findUserByName(any())).thenReturn(Optional.of(user));
    }

    @Test
    public void givenSimpleData_whenServiceRunsFindByName_thenUserDaoPunsFindByName() {
        //w
        userService.findUserByName("exaple");
        //t
        Mockito.verify(userDao, times(1)).findUserByName(any());
    }

    @Test(expected = UserException.class)
    public void givenEmptyShareResult_whenServiceRunsFindUserByName_thenUserGetException$NAME_IS_INCORRECT() {
        //g
        when(userDao.findUserByName(any())).thenReturn(Optional.empty());
        //w
        userService.findUserByName("exaple");
        //t exception
    }

    @Test(expected = UserException.class)
    public void givenSimpleData_whenServiceRunsFindUserByNameWithNull_thenUserGetException$NAME_IS_INCORRECT() {
        //g
        when(userDao.findUserByName(any())).thenReturn(Optional.empty());
        //w
        userService.findUserByName(null);
        //t exception
    }

    @Test(expected = UserException.class)
    public void givenUserWhichExist_whenServiceRunsSaveUserAndUserExists_thenUserGetException$USER_ALREADY_EXIST() {
        //g
        when(userDao.findUserByName(any())).thenReturn(Optional.of(user));
        //w
        userService.saveUser(registerDto);
        //t exception
    }
}
